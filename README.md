Provision www: $ ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts -e "hosts=www"

Provision admin: $ ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts -e "hosts=admin"

Provision DB: $ ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts -e "hosts=db"

Provision producción: $ ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts -e "hosts=prod"

Provision www, admin, producción and DB: $ ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts -e "hosts=infraestructure"